import { useState } from "react";
import './Colors.css';

import ColorButton from "./ColorButton";

const Colors = () => {

  const colorsImages = [
    {
      id: 1,
      name: 'Красный',
      src: 'img/color1.png',
      alt: 'Красный смартфон Apple iPhone 13',
    },
    {
      id: 2,
      name: 'Зеленый',
      src: 'img/color2.png',
      alt: 'Зеленый смартфон Apple iPhone 13',
    },
    {
      id: 3,
      name: 'Розовый',
      src: 'img/color3.png',
      alt: 'Розовый смартфон Apple iPhone 13',
    },
    {
      id: 4,
      name: 'Синий',
      src: 'img/color4.png',
      alt: 'Синий смартфон Apple iPhone 13',
    },
    {
      id: 5,
      name: 'Белый',
      src: 'img/color5.png',
      alt: 'Белый смартфон Apple iPhone 13',
    },
    {
      id: 6,
      name: 'Черный',
      src: 'img/color6.png',
      alt: 'Черный смартфон Apple iPhone 13',
    },
  ];

  const [activeColorImageId, setActiveColorImageId] = useState(1);

  const changeActiveColor = (id) => {
    setActiveColorImageId(id);
  };
  
  return (
    <div className="product-info__subsection">
      <h4 className="product-info__subtitle">Цвет товара: { colorsImages[activeColorImageId - 1].name }</h4>

      <div className="product-info__gallery">
        {colorsImages.map((item) => {
          return <ColorButton 
                    key={item.id}
                    id={item.id}
                    name={item.name}
                    src={item.src}
                    alt={item.alt}
                    active={activeColorImageId === item.id}
                    changeActiveColor={changeActiveColor} 
                  />
        })}
      </div>
    </div>
  );
}

export default Colors;