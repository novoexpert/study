import { Link } from 'react-router-dom';
import './HomePage.css';

import Header from "../../Header/Header";
import Footer from "../../Footer/Footer";
import Nav from "../../Nav/Nav";

const HomePage = () => {
  return (
    <>
      <Header />

      <Nav />

      <div className="container">
        <h1 className="homePage-title">Главная страница</h1>
        <Link className="homePage-link" to="/product">На страницу товара</Link>
      </div>

      <Footer />
    </>
  );
};

export default HomePage;