import React from "react";
import "./PageProduct.css";

import Header from "../../Header/Header";
import Colors from "../../Colors/Colors";
import Configs from "../../Configs/Configs";
import ProductReviews from '../../ProductReviews/ProductReviews';
import Sidebar from "../../Sidebar/Sidebar";
import ProductAddReview from '../../ProductAddReview/ProductAddReview';
import Footer from '../../Footer/Footer';

const PageProduct = ({ product }) => {
  return (
    <div>
      <Header />
      <div className="container">
        <nav className="nav">
          <a href="/">Электроника</a> {">"} <a href="/">Смартфоны и гаджеты</a>{" "}
          {">"} <a href="/">Мобильные телефоны</a> {">"} <a href="/">Apple</a>
        </nav>

        <main>
          <section>
            <h2 className="title">Смартфон Apple iPhone 13</h2>
            <div className="img-large">
              <img
                className="img-large__img"
                src="./img/image-1.png"
                alt="cмартфон Apple iPhone 13"
              />
              <img
                className="img-large__img"
                src="./img/image-2.png"
                alt="cмартфон Apple iPhone 13"
              />
              <img
                className="img-large__img"
                src="./img/image-3.png"
                alt="cмартфон Apple iPhone 13"
              />
              <img
                className="img-large__img"
                src="./img/image-4.png"
                alt="cмартфон Apple iPhone 13"
              />
              <img
                className="img-large__img"
                src="./img/image-5.png"
                alt="cмартфон Apple iPhone 13"
              />
            </div>
          </section>

          <section className="product">
            <div className="product-info">
              <Colors />
              <div>
                <Configs />

                <div className="product-info__subsection">
                  <h4>Характеристики товара</h4>
                  <ul className="product-features">
                    <li className="product-features__item">
                      Экран: <b>6.1</b>
                    </li>
                    <li className="product-features__item">
                      Встроенная память: <b>128 ГБ</b>
                    </li>
                    <li className="product-features__item">
                      Операционная система: <b>iOS 15</b>
                    </li>
                    <li className="product-features__item">
                      Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b>
                    </li>
                    <li className="product-features__item">
                      Процессор:
                      <a href="https://ru.wikipedia.org/wiki/Apple_A15">
                        <b>Apple A15 Bionic</b>
                      </a>
                    </li>
                    <li className="product-features__item">
                      Вес: <b>173 г</b>
                    </li>
                  </ul>
                </div>

                <div className="product-info__description">
                  <div>
                    <h4 className="product-info__description-title">
                      Описание
                    </h4>
                    <div>
                      Наша самая совершенная система двух камер. <br />
                      Особый взгляд на прочность дисплея. <br />
                      Чип, с которым всё супербыстро. <br />
                      Аккумулятор держится заметно дольше. <br />
                      <i>iPhone 13 - сильный мира всего.</i>
                    </div>

                    <p>
                      Мы разработали совершенно новую схему расположения и
                      развернулиобъективы на 45 градусов. Благодаря этому внутри
                      корпуса поместилась нашалучшая система двух камер с
                      увеличенной матрицей широкоугольной камеры.Кроме того, мы
                      освободили место для системы оптической
                      стабилизацииизображения сдвигом матрицы. И повысили
                      скорость работы матрицы насверхширокоугольной камере.
                    </p>

                    <p>
                      Новая сверхширокоугольная камера видит больше деталей в
                      тёмных областяхснимков. Новая широкоугольная камера
                      улавливает на 47% больше света для более качественных
                      фотографий и видео. Новая оптическая стабилизация
                      сосдвигом матрицы обеспечит чёткие кадры даже в
                      неустойчивом положении.
                    </p>

                    <p>
                      Режим «Киноэффект» автоматически добавляет великолепные
                      эффекты перемещенияфокуса и изменения резкости. Просто
                      начните запись видео. Режим «Киноэффект»будет удерживать
                      фокус на объекте съёмки, создавая красивый эффект
                      размытиявокруг него. Режим «Киноэффект» распознаёт, когда
                      нужно перевести фокус на другогочеловека или объект,
                      который появился в кадре. Теперь ваши видео будут
                      смотретьсякак настоящее кино.
                    </p>
                  </div>
                </div>
              </div>

              <section className="product-table">
                <h4>Сравнение моделей</h4>
                <table className="product-table__inner">
                  <thead>
                    <tr className="product-table__sell">
                      <th>Модель</th>
                      <th>Вес</th>
                      <th>Высота</th>
                      <th>Ширина</th>
                      <th>Толщина</th>
                      <th>Чип</th>
                      <th>Объём памяти</th>
                      <th>Аккумулятор</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className="product-table__sell-data">
                      <td>iPhone 11</td>
                      <td>194 грамма</td>
                      <td>150.9 мм</td>
                      <td>75.7 мм</td>
                      <td>8.3 мм</td>
                      <td>A13 Bionic chip</td>
                      <td>до 128 Гб</td>
                      <td>до 17 часов</td>
                    </tr>
                    <tr className="product-table__sell-data">
                      <td>iPhone 12</td>
                      <td>164 грамма</td>
                      <td>146.7 мм</td>
                      <td>71.5 мм</td>
                      <td>7.4 мм</td>
                      <td>A14 Bionic chip</td>
                      <td>до 256 Гб</td>
                      <td>до 19 часов</td>
                    </tr>
                    <tr className="product-table__sell-data">
                      <td>iPhone 13</td>
                      <td>174 грамма</td>
                      <td>146.7 мм</td>
                      <td>71.5 мм</td>
                      <td>7.65 мм</td>
                      <td>A15 Bionic chip</td>
                      <td>до 512 Гб</td>
                      <td>до 19 часов</td>
                    </tr>
                  </tbody>
                </table>
              </section>

                            <section className="review">
                {/* <div className="review__inner">
                  <div>
                    <img
                      className="review__inner-img"
                      src="./img/review-1.jpeg"
                      alt="Отзыв Марк Г."
                      height="200px"
                    />
                  </div>
                  <div className="review__inner-content">
                    <p>
                      <b>Марк Г.</b>
                    </p>
                    <div className="stars">
                      <p>
                        <img src="./img/star.png" alt="star-5" height="30" />
                      </p>
                      <p>
                        <img src="./img/star.png" alt="star-5" height="30" />
                      </p>
                      <p>
                        <img src="./img/star.png" alt="star-5" height="30" />
                      </p>
                      <p>
                        <img src="./img/star.png" alt="star-5" height="30" />
                      </p>
                      <p>
                        <img src="./img/star.png" alt="star-5" height="30" />
                      </p>
                    </div>
                    <p>
                      <b>Опыт использования:</b> менее месяца
                    </p>
                    <p>
                      <b>Достоинства:</b>
                      <br />
                      это мой первый айфон после после огромного количества
                      телефонов на андроиде. всёплавно, чётко и красиво.
                      довольно шустрое устройство. камера весьма неплохая,ширик
                      тоже на высоте.
                    </p>
                    <p>
                      <b>Недостатки:</b>
                      <br />к самому устройству мало имеет отношение, но перенос
                      данных с андроида - адскаявещь, а если нужно переносить
                      фото с компа, то это только через itunes, которыйурезает
                      качество фотографий исходное
                    </p>
                  </div>
                </div>

                <hr className="hr-dashed" />

                <div className="review__inner">
                  <div>
                    <img
                      className="review__inner-img"
                      src="./img/review-2.jpeg"
                      alt="Отзыв Коваленко"
                    />
                  </div>
                  <div className="review__inner-content">
                    <p>
                      <b>Валерий Коваленко</b>
                    </p>
                    <div className="stars">
                      <p>
                        <img src="./img/star.png" alt="star-4" height="30" />
                      </p>
                      <p>
                        <img src="./img/star.png" alt="star-4" height="30" />
                      </p>
                      <p>
                        <img src="./img/star.png" alt="star-4" height="30" />
                      </p>
                      <p>
                        <img src="./img/star.png" alt="star-4" height="30" />
                      </p>
                      <p>
                        <img
                          src="./img/star_empty.png"
                          alt="star-4"
                          height="30"
                        />
                      </p>
                    </div>

                    <p>
                      <b>Опыт использования:</b> менее месяца
                    </p>
                    <p>
                      <b>Достоинства:</b>
                      <br />
                      OLED экран, Дизайн, Система камер, Шустрый А15, Заряд
                      держит долго
                    </p>
                    <p>
                      <b>Недостатки:</b>
                      <br />
                      Плохая ремонтопригодность
                    </p>
                  </div>
                </div> */}

                <ProductReviews reviews={product.reviews} />
              <ProductAddReview />
              </section>
            </div>



            <section className="product-offer">
              
              <Sidebar />
                <div className="product-adv">
      <div className="product-adv__note">Реклама</div>
      <div className="product-adv__banners">
        <div className="product-adv__banner">Здесь могла бы быть ваша реклама</div>
        <div className="product-adv__banner">Здесь могла бы быть ваша реклама</div>
      </div>  
    </div>
            </section>
          </section>
        </main>
      </div>

      <Footer />
   
    </div>
  );
}

export default PageProduct
