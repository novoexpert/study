import { useState } from 'react';
import './Sidebar.css';

const Sidebar = () => {

  const [cartBtnIsActive, setCartBtnIsActive] = useState(false);

  const cartBtnClasses = cartBtnIsActive ? 'product-buy__btn product-buy__btn-active' : 'product-buy__btn'

  const handleClickCartBtn = () => {
    setCartBtnIsActive(!cartBtnIsActive);
  };

  return (
    <section className="product-offer">
      <div className="product-buy">
        <div className="product-buy__old">
          <span className="product-buy__old-price">75 990₽</span>
          <span className="product-buy__old-percent">-8%</span>
        </div>
        <div className="product-buy__new">
          67 990₽
        </div>
        <div className="product-buy__get">
          <p>Самовывоз в четверг, 1 сентября — <span className="bold">бесплатно</span></p>
          <p>Курьером в четверг, 1 сентября — <span className="bold">бесплатно</span></p>
        </div>
        <button className={cartBtnClasses} onClick={handleClickCartBtn}>
          {cartBtnIsActive ? 'Товар добавлен' : 'Добавить в корзину'}
        </button>
      </div>
    </section>
  )
};

export default Sidebar;