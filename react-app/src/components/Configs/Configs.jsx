import { useState } from 'react';
import ConfigButton from "./ConfigButton";

const Configs = () => {

  const configButtons = [
    {
      id: 1,
      memory: '128 ГБ',
    },
    {
      id: 2,
      memory: '256 ГБ',
    },
    {
      id: 3,
      memory: '512 ГБ',
    },
  ];

  const [activeConfigButtonId, setActiveConfigButtonId] = useState(1);

  const changeActiveMemory = (id) => {
    setActiveConfigButtonId(id)
  }

  return (
    <div className="product-info__subsection">
      <h4 className="product-info__subtitle">Конфигурация памяти: { configButtons[activeConfigButtonId - 1].memory }</h4>
      <p>
        {configButtons.map((item) => {
          return <ConfigButton key={item.id} id={item.id} memory={item.memory} changeActiveMemory={changeActiveMemory} activeConfigButtonId={activeConfigButtonId === item.id}
          /> 
        })}
      </p>
    </div>
  );
}

export default Configs;