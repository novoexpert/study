const ConfigButton = ({ id, memory, changeActiveMemory, activeConfigButtonId }) => {

  const handleClick = () => {
    changeActiveMemory(id)
  }

  const itemClassName = activeConfigButtonId 
                          ? 'product-info__btn activ'
                          : 'product-info__btn';
  


  return (
    <button className={itemClassName} onClick={handleClick}>{ memory }</button>
  );
}

export default ConfigButton;