import './Footer.css';

const Footer = () => {
  return (
    <footer className="footer">
        <div className="footer__inner">
          <div>
            <p>
              <b>
                © ООО «<span className="accent">Мой</span>Маркет», 2018-2022.
              </b>
            </p>
            <p>
              Для уточненияинформации звоните по номеру &nbsp;
              <a href="tel:+79000000000">+7 900 000 0000</a>, <br />а
              предложения по сотрудничеству отправляйте на почту &nbsp;
              <a href="mailto:">partner@mymarket.com</a>
            </p>
          </div>
          <p>
            <a href="#top">Наверх</a>
          </p>
        </div>
      </footer>
  );
};

export default Footer;