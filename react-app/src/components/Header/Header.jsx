import { Link } from 'react-router-dom';
import './Header.css';

const Header = () => {
  return (
    <header className="header">
        <div className="container header-container">
        <Link to='/'>
          <div>
            <img
              className="logo"
              src="./img/apple-touch-icon.png"
              alt="logo"
              height="35"
            />
            <h1 className="header__title">
              <span className="accent">Мой</span>Маркет
            </h1>
          </div>
          </Link>


          <div className="icons">
            <div className="icon">
              <img className="icon__img" src="./img/cart.svg" alt="Корзина" />
              <span className="icon__indicator" id="cart-indicator">
                1
              </span>
            </div>
          </div>
        </div>
      </header>
  );
}

export default Header;