import { Routes, Route } from 'react-router-dom';


import HomePage from './components/pages/HomePage/HomePage'
import PageProduct from './components/pages/PageProduct/PageProduct'
import { product } from './mocks/product';

const App = () => {
  return (
    <div className="App">
      <Routes>
        <Route index element={<HomePage />}></Route>
        <Route path='product' element={<PageProduct product={product} />}></Route>
      </Routes>
    </div>
  );
}

export default App;
